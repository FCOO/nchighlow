#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Purpose: Compute the locations of high- and low pressure centers, then store
    them in NetCDF format. Intended for visualization. The script will remove
    any extrema that are too close to another, with priority for most extreme
    systems (lows before highs).

Usage: Called from command line. (run nchighlow.py -h for further information)
    python nchighlow.py [options] <infile> <outfile>

NOTE: It could be considered to read the maximum windspeed (if available)
    within the (low) pressure area (e.g. window size) to categorize or rank
    the pressure systems even more.
"""

__author__ = "Brian Højen-Sørensen"
__contact__ = "brs@fcoo.dk"
__copyright__ = "Copyright 2023, Danish Defence"
__license__ = "MIT License"
__version__ = '1.0.0'

import argparse
import json
import os
import shutil
import tempfile
import time
from typing import Any, Tuple

import netCDF4  # type: ignore
import numpy as np
from scipy.ndimage import maximum_filter, minimum_filter  # type: ignore
from ncutils import ncutils

RADIUS = 6371  # Earth radius
HPRESS = 1015  # Lowest value for high pressures
LPRESS = 1020  # Highest value for low pressures


def haversine_distance(lon1: np.ndarray, lat1: np.ndarray, lon2: np.ndarray, lat2: np.ndarray) -> np.ndarray:
    """Compute Haversine distance"""
    dlat = np.radians(lat2 - lat1) / 2.0
    dlon = np.radians(lon2 - lon1) / 2.0
    a = np.sin(dlat) * np.sin(dlat) + np.cos(np.radians(lat1)) * np.cos(np.radians(lat2)) * np.sin(dlon) * np.sin(dlon)
    a = np.where(a > 1, 1.0, a)
    d = 2.0 * RADIUS * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
    return d


def sort_and_limit(
    vals: np.ndarray, x: np.ndarray, y: np.ndarray, maxext: int, lowest_first: bool
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    # Sort and limit to maxext
    if lowest_first:
        idx = np.argsort(vals)
    else:
        idx = np.argsort(vals)[::-1]
    if len(idx) > maxext:
        idx = idx[:maxext]
    vals_out = vals[idx]
    x_out = x[idx]
    y_out = y[idx]
    return vals_out, x_out, y_out


def remove_close_extrema(
    x: np.ndarray, y: np.ndarray, vals: np.ndarray, distance: float
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    idx = [0]
    for j in range(1, len(vals)):
        d = haversine_distance(x[idx], y[idx], x[j], y[j])
        if min(d) > distance:
            idx.append(j)
    return x[idx], y[idx], vals[idx]


def find_highlows(
    infile: str,
    distance: float = 500,
    maxext: int = 50,
    resolution: float = 25.0,
    mode: str = 'nearest',
    edge: int = 15,
) -> list[dict[str, Any]]:
    # Read NetCDF file
    nc = netCDF4.Dataset(infile, mode='r')

    # Access latitudes, longitudes, time, and mslp variables
    lon = ncutils.longitude(nc)
    lat = ncutils.latitude(nc)
    tm = ncutils.temporal(nc)
    mslpvar = ncmslp(nc)
    if mslpvar.units == 'Pa':
        cfactor = 0.01
    elif mslpvar.units == 'hPa':
        cfactor = 1.0
    else:
        raise ValueError('Units must be in hPa or Pa: unit is: ' + mslpvar.units)

    grid_resolution = find_spatial_resolution(lon=lon, lat=lat)
    stride = find_grid_stride(calculation_resolution=resolution, grid_resolution=grid_resolution)
    lons, lats = get_lonlats_inner(lon=lon, lat=lat, edge=edge, stride=stride)
    window = get_calculation_window(lons=lons, lats=lats)

    extrema = []
    for i in range(len(tm)):
        mslp = cfactor * get_data_inner(mslpvar[i], edge, stride)

        lmin, lmax = calculate_extrema(mslp, mode=mode, window=window)

        xlows = lons[lmin]
        ylows = lats[lmin]
        lvals = mslp[lmin]

        xhighs = lons[lmax]
        yhighs = lats[lmax]
        hvals = mslp[lmax]

        lvals, xlows, ylows = sort_and_limit(vals=lvals, x=xlows, y=ylows, maxext=maxext, lowest_first=True)
        hvals, xhighs, yhighs = sort_and_limit(vals=hvals, x=xhighs, y=yhighs, maxext=maxext, lowest_first=False)

        xlows, ylows, lvals = remove_close_extrema(x=xlows, y=ylows, vals=lvals, distance=distance)
        xhighs, yhighs, hvals = remove_close_extrema(x=xhighs, y=yhighs, vals=hvals, distance=distance)

        lows: list[dict[str, float]] = []
        for xlow, ylow, lval in zip(xlows, ylows, lvals):
            if lval <= LPRESS:
                lows.append({"lon": float(xlow), "lat": float(ylow), "pressure": float(lval)})
        highs: list[dict[str, float]] = []
        for xhigh, yhigh, hval in zip(xhighs, yhighs, hvals):
            if hval >= HPRESS:
                highs.append({"lon": float(xhigh), "lat": float(yhigh), "pressure": float(hval)})

        extrema_timestep = {
            "time": tm[i],
            "lows": lows,
            "highs": highs,
        }
        extrema.append(extrema_timestep)

    return extrema


def find_spatial_resolution(lon: np.ndarray, lat: np.ndarray) -> float:
    if len(lon.shape) > 1:  # 2d input coordinates
        res = haversine_distance(lon[0, 0], lat[0, 0], lon[1, 0], lat[1, 0])
    else:  # 1d input coordinates
        res = haversine_distance(lon[0], lat[0], lon[0], lat[1])
    return float(res)


def find_grid_stride(calculation_resolution: float, grid_resolution: float) -> int:
    return max(int(round(calculation_resolution / grid_resolution)), 1)


def get_lonlats_inner(lon: np.ndarray, lat: np.ndarray, edge: int, stride: int) -> Tuple[np.ndarray, np.ndarray]:
    # Avoid edges as the filter can't seem to handle them correctly
    if len(lon.shape) > 1:  # 2d input coordinates
        lons = lon[edge:-edge:stride, edge:-edge:stride]
        lats = lat[edge:-edge:stride, edge:-edge:stride]
    else:  # 1d input coordinates
        lons, lats = np.meshgrid(lon[edge:-edge:stride], lat[edge:-edge:stride])
    return lons, lats


def get_data_inner(data: np.ndarray, edge: int, stride: int) -> np.ndarray:
    return data[edge:-edge:stride, edge:-edge:stride]


def get_calculation_window(lons: np.ndarray, lats: np.ndarray, window_size_meters: float = 5000) -> int:
    resolution = haversine_distance(lons[0, 0], lats[0, 0], lons[1, 0], lats[1, 0])
    return int(window_size_meters / resolution)


def calculate_extrema(
    mat: np.ndarray, mode: str = 'constant', window: int = 150
) -> Tuple[
    Tuple[np.ndarray[Any, np.dtype[np.signedinteger[Any]]], ...],
    Tuple[np.ndarray[Any, np.dtype[np.signedinteger[Any]]], ...],
]:
    """
    find the indices of local extrema in the input array.
    mode : reflect, constant, nearest, mirror, wrap.
    window : number of highs and lows detected.
    """
    mn = minimum_filter(mat, size=window, mode=mode)
    mx = maximum_filter(mat, size=window, mode=mode)

    # Return the indices of the maxima, minima
    return np.nonzero(mat == mn), np.nonzero(mat == mx)


def ncmslp(nc: netCDF4.Dataset) -> netCDF4.Variable:
    """\
    Extracts mean sea-level pressure variable from a netcdf file object.
    Returns None if mean sea-level pressure not present.
    """
    p = None
    for v in nc.variables:
        ncvar = nc.variables[v]
        if ismslp(ncvar):
            p = ncvar
    return p


def ismslp(ncvar: netCDF4.Variable) -> bool:
    """Returns True if input is mean sea-level pressure variable."""
    unam = [
        'mean sea level pressure',
        'air_pressure_at_sea_level',
        'pressure',
        'air_pressure_at_mean_sea_level',
        'mean sea-level pressure',
    ]
    if hasattr(ncvar, 'long_name') and getattr(ncvar, 'long_name').lower() in unam:
        return True
    elif hasattr(ncvar, 'standard_name') and getattr(ncvar, 'standard_name').lower() in unam:
        return True
    return False


def create_geojson(highlows: list[dict[str, Any]]) -> dict[str, Any]:
    features: list = []
    time_fmt = "%Y-%m-%dT%H:%M:%SZ"

    for highlow in highlows:
        tm = highlow["time"]
        for low in highlow["lows"]:
            feature = {
                "type": "Feature",
                "geometry": {"type": "Point", "coordinates": [low["lon"], low["lat"]]},
                "properties": {"pressure": low["pressure"], "time": tm.strftime(time_fmt), "type": "low"},
            }
            features.append(feature)

        for high in highlow["highs"]:
            feature = {
                "type": "Feature",
                "geometry": {"type": "Point", "coordinates": [high["lon"], high["lat"]]},
                "properties": {"pressure": high["pressure"], "time": tm.strftime(time_fmt), "type": "high"},
            }
            features.append(feature)
    return {"type": "FeatureCollection", "features": features}


def save_geojson(highlows: list[dict[str, Any]], outfile: str) -> None:
    json.dump(create_geojson(highlows), open(outfile, 'w'))


def save_text(highlows: list[dict[str, Any]], outfile_template: str) -> list[str]:
    outfiles = []
    for highlow in highlows:
        outfile_time = outfile_template.format(date=highlow["time"])
        outfiles.append(outfile_time)
        with open(outfile_time, 'w') as f:
            if len((highlow["lows"])) > 0:
                f.write("L")
                for low in highlow["lows"]:
                    f.write(f" {low['lon']} {low['lat']} {low['pressure']}")
                f.write("\n")
            if len((highlow["highs"])) > 0:
                f.write("H")
                for high in highlow["highs"]:
                    f.write(f" {high['lon']} {high['lat']} {high['pressure']}")
                f.write("\n")
    return outfiles


def _find_max_number_of_extrema(highlows: list[dict[str, Any]]) -> int:
    maxext = 0
    for highlow in highlows:
        maxext = max(maxext, len(highlow["lows"]) + len(highlow["highs"]))
    return maxext


def save_netcdf(highlows: list[dict[str, Any]], infile: str, outfile: str) -> None:
    maxext = _find_max_number_of_extrema(highlows)

    nc = netCDF4.Dataset(infile, mode='r')
    with tempfile.TemporaryDirectory() as tmpdir:
        tmpfile = os.path.join(tmpdir, 'tmp.nc')
        shutil.copy(infile, tmpfile)

        ncout = netCDF4.Dataset(tmpfile, mode='a')
        ncout.createDimension('ext', maxext)
        ncout.createDimension('str', 8)
        mslext = ncout.createVariable('mslext', 'S1', ('time', 'ext', 'str'))
        mslext.units = 'hPa'
        mslext.long_name = 'mean sealevel pressure extrema'
        mslext.coordinates = 'latext lonext'

        fill_value = netCDF4.default_fillvals['f4']
        latext = ncout.createVariable(
            'latext',
            'f4',
            (
                'time',
                'ext',
            ),
            fill_value=fill_value,
        )
        latext.units = 'degrees_north'
        latext.long_name = 'mslp extrema latitudes'
        lonext = ncout.createVariable(
            'lonext',
            'f4',
            (
                'time',
                'ext',
            ),
            fill_value=fill_value,
        )
        lonext.units = 'degrees_east'
        lonext.long_name = 'mslp extrema longitudes'

        for i, highlow in enumerate(highlows):
            # Create LaTeX lines
            lines = []
            for low in highlow["lows"]:
                lines.append('{0: <8}'.format('L:' + str(int(low["pressure"]))))
            for high in highlow["highs"]:
                lines.append('{0: <8}'.format('H:' + str(int(high["pressure"]))))

            ylows = [low["lat"] for low in highlow["lows"]]
            xlows = [low["lon"] for low in highlow["lows"]]
            yhighs = [high["lat"] for high in highlow["highs"]]
            xhighs = [high["lon"] for high in highlow["highs"]]

            numext = len(lines)
            mslext[i, :numext] = netCDF4.stringtochar(np.array([lines], 'S'))
            latext[i, :numext] = np.hstack((ylows, yhighs))
            lonext[i, :numext] = np.hstack((xlows, xhighs))

        # Append history information and close output file
        nowstr = time.strftime('%Y-%m-%d %H:%M:%S (GM)', time.gmtime())
        if hasattr(ncout, 'history'):
            ncout.history += '\n' + "%s: High and low pressure extrema computed from %s." % (nowstr, infile)
        else:
            ncout.history = "%s: High and low pressure extrema computed from %s." % (nowstr, infile)

        nc.close()
        ncout.close()
        shutil.move(tmpfile, outfile)


def main():
    # Get my own name
    scriptname = os.path.basename(__file__)

    # Get command line arguments
    parser = argparse.ArgumentParser(
        description="""
                     Compute the locations of high- and low pressure centers
                     """
    )

    parser.add_argument(dest="infile", type=str, action="store", default="", help="path to source file.")

    parser.add_argument(dest="outfile", type=str, action="store", default="", help="filename of the output file.")

    parser.add_argument(
        "-f",
        "--format",
        metavar='format',
        dest="format",
        type=str,
        default='geojson',
        help="Output format. Options are: geojson, netcdf or text.",
    )

    parser.add_argument(
        "-d",
        "--distance",
        metavar='distance',
        dest="distance",
        type=float,
        default=500,
        help="minimum distance between extrema in km. Default is 500 km.",
    )

    parser.add_argument(
        "-x",
        "--maxext",
        metavar='maxnum',
        dest="maxext",
        type=int,
        default=50,
        help="Maximum number of extrema detected. Default is 50 of each.",
    )

    parser.add_argument(
        "-r",
        "--resolution",
        metavar='resolution',
        dest="resolution",
        type=float,
        default=25.0,
        help="Preferred Model resolution in km, default = 25 km. Can be used to control amount of extremas detected.",
    )

    parser.add_argument(
        "-m",
        "--mode",
        metavar='mode',
        dest="mode",
        type=str,
        default='nearest',
        help=(
            "Mode for handling boundaries when finding extrema. Options are: reflect, constant, nearest, mirror, wrap."
        ),
    )

    parser.add_argument(
        "-e",
        "--edge",
        metavar='edge',
        dest="edge",
        type=int,
        default=15,
        help="Number of edgepoints excluded in the computation. Default is 15.",
    )

    parser.add_argument(
        "-O",
        "--overwrite",
        dest="overwrite",
        action="store_true",
        default=False,
        help="Overwrite output file if it exist (or append to input file).",
    )

    args = parser.parse_args()
    infile = args.infile
    outfile = args.outfile
    distance = args.distance
    maxext = args.maxext
    resolution = args.resolution
    mode = args.mode
    edge = args.edge
    format_ = args.format
    overwrite = args.overwrite

    # Does the source file actually exist
    if not os.path.isfile(infile):
        exit(scriptname + ': input file: ' + infile + ': no such file.')

    # Check if we are trying to overwrite any files (unless overwrite = True)
    if not overwrite:
        if os.path.isfile(outfile):
            exit(scriptname + ': ' + outfile + ': file exists. Choose different name or use -O / --overwrite')

    highlows: list[dict[str, Any]] = find_highlows(
        infile=infile, distance=distance, maxext=maxext, resolution=resolution, mode=mode, edge=edge
    )
    if format_ == 'geojson':
        save_geojson(highlows, outfile)
    elif format_ == 'netcdf':
        save_netcdf(highlows, infile, outfile)
    elif format_ == 'text':
        save_text(highlows, outfile)
    else:
        exit(scriptname + ': ' + format_ + ': unknown output format.')


if __name__ == '__main__':
    exit(main())
