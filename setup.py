import codecs
import os
import re
from setuptools import setup, find_packages  # type: ignore

with open("README.md", "r") as fh:
    long_description = fh.read()

here = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    with codecs.open(os.path.join(here, *parts), 'r') as fp:
        return fp.read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(
    name='nchighlow',
    version=find_version(".", "nchighlow.py"),
    author='Brian Højen-Sørensen',
    author_email='brs@fcoo.dk',
    description="tool for finding high and low pressures in NetCDF files",
    long_description=long_description,
    long_description_content_type="text/markdown",
    scripts=['nchighlow.py'],
    packages=find_packages(),
    url='https://gitlab.com/FCOO/nchighlow',
    install_requires=[
        'netCDF4',
        'ncutils-fcoo',
        'numpy',
        'scipy',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ],
)
