import numpy as np
import pytest

import nchighlow


# Test cases for haversine_distance
@pytest.mark.parametrize(
    "lon1, lat1, lon2, lat2, expected",
    [
        (0, 0, 0, 0, 0),
        (0, 0, 1, 1, 157.24938127194397),
        (-75, 40, -80, 45, 690.440334401671),
    ],
)
def test_haversine_distance(lon1, lat1, lon2, lat2, expected):
    result = nchighlow.haversine_distance(np.array(lon1), np.array(lat1), np.array(lon2), np.array(lat2))
    assert np.isclose(result, expected, rtol=1e-5)


# Test cases for sort_and_limit
@pytest.mark.parametrize(
    "vals, x, y, maxext, lowest_first, expected",
    [
        (
            np.array([3, 1, 4, 5]),
            np.array([0, 1, 2, 3]),
            np.array([0, 1, 2, 3]),
            3,
            True,
            (np.array([1, 3, 4]), np.array([1, 0, 2]), np.array([1, 0, 2])),
        ),
        (
            np.array([3, 1, 4, 5]),
            np.array([0, 1, 2, 3]),
            np.array([0, 1, 2, 3]),
            3,
            False,
            (np.array([5, 4, 3]), np.array([3, 2, 0]), np.array([3, 2, 0])),
        ),
    ],
)
def test_sort_and_limit(vals, x, y, maxext, lowest_first, expected):
    result = nchighlow.sort_and_limit(vals, x, y, maxext, lowest_first)
    assert np.array_equal(result, expected)


# Test cases for remove_close_extrema
@pytest.mark.parametrize(
    "x, y, vals, distance, expected",
    [
        (
            np.array([0, 1, 2, 3]),
            np.array([0, 1, 2, 3]),
            np.array([1, 2, 3, 4]),
            200,
            (np.array([0, 2]), np.array([0, 2]), np.array([1, 3])),
        ),
    ],
)
def test_remove_close_extrema(x, y, vals, distance, expected):
    result = nchighlow.remove_close_extrema(x, y, vals, distance)
    assert np.array_equal(result, expected), result


@pytest.mark.parametrize(
    "lon, lat, expected_resolution",
    [
        (np.array([[0, 1], [0, 1]]), np.array([[0, 0], [1, 1]]), 111.19492664455873),
        (np.array([0, 1]), np.array([0, 1]), 111.19492664455873),
    ],
)
def test_find_spatial_resolution(lon, lat, expected_resolution):
    assert nchighlow.find_spatial_resolution(lon, lat) == pytest.approx(expected_resolution)


@pytest.mark.parametrize(
    "calculation_resolution, grid_resolution, expected_stride",
    [
        (25.0, 50.0, 2),
        (25.0, 100.0, 4),
    ],
)
def test_find_grid_stride(calculation_resolution, grid_resolution, expected_stride):
    assert nchighlow.find_grid_stride(calculation_resolution, grid_resolution) == expected_stride


@pytest.mark.parametrize(
    "lon, lat, edge, stride, expected_lons, expected_lats",
    [
        (
            np.array([[0, 1, 2], [0, 1, 2], [0, 1, 2]]),
            np.array([[0, 0, 0], [1, 1, 1], [2, 2, 2]]),
            1,
            1,
            np.array([[1]]),
            np.array([[1]]),
        ),
        (
            np.array([0, 1, 2]),
            np.array([0, 1, 2]),
            1,
            1,
            np.array([[1]]),
            np.array([[1]]),
        ),
    ],
)
def test_get_lonlats_inner(lon, lat, edge, stride, expected_lons, expected_lats):
    lons, lats = nchighlow.get_lonlats_inner(lon, lat, edge, stride)
    assert np.array_equal(lons, expected_lons), lons
    assert np.array_equal(lats, expected_lats), lats


@pytest.mark.parametrize(
    "data, edge, stride, expected_data",
    [
        (np.array([[0, 1, 2], [0, 1, 2], [0, 1, 2]]), 1, 1, np.array([[1]])),
    ],
)
def test_get_data_inner(data, edge, stride, expected_data):
    result = nchighlow.get_data_inner(data, edge, stride)
    assert np.array_equal(result, expected_data)


@pytest.mark.parametrize(
    "lons, lats, expected_window",
    [
        (
            np.array([[0, 1], [0, 1]]),
            np.array([[0, 0], [1, 1]]),
            44,
        ),
    ],
)
def test_get_calculation_window(lons, lats, expected_window):
    assert nchighlow.get_calculation_window(lons, lats) == expected_window


@pytest.mark.parametrize(
    "mat, mode, window, expected_min_indices, expected_max_indices",
    [
        (
            np.array([[0, 1, 2], [3, 4, 5], [6, 7, 8]]),
            'nearest',
            3,
            (np.array([0]), np.array([0])),
            (np.array([2]), np.array([2])),
        ),
    ],
)
def test_calculate_extrema(mat, mode, window, expected_min_indices, expected_max_indices):
    min_indices, max_indices = nchighlow.calculate_extrema(mat, mode, window)
    assert np.array_equal(min_indices, expected_min_indices), min_indices
    assert np.array_equal(max_indices, expected_max_indices), max_indices
